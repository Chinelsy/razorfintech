﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinance
{
    class Program
    {
        async static Task Main(string[] args)
        {
            await Getting();
            Console.WriteLine("please do wait for your response");
        }

        public async static Task Getting()
        {
            Console.WriteLine("cool down we are getting your verification...");

            var profile = await Service.GetUserValidationDetails("sammy@yahoo.co.uk");
            var valid = await Service.Login("chikki@yahoo.co.uk", "chikki");

            var userValidationDetails = profile.Split();
            if (valid == true)
            {
                Console.WriteLine($"FullName:  {userValidationDetails[0]}");
                Console.WriteLine($"AccountNumber:  {userValidationDetails[1]}");
                Console.WriteLine($"Email:  {userValidationDetails[2]}");
            };
            
        }

    }
}
