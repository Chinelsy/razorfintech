﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinance
{
      static class Service
      {
      
      

          private static IEnumerable<UserValidation> allUserValidation = new List<UserValidation>()
          {
            new UserValidation{FullName = "Dike Chikki",        AccountNumber  = 0123456789,    Email = "chikki@yahoo.co.uk"},
            new UserValidation{FullName = "John Dara",          AccountNumber  = 0123456781,    Email = "dara@yahoo.co.uk"},
            new UserValidation{FullName = "Agu Lucy",           AccountNumber  = 0123456782,    Email = "lucy@yahoo.co.uk"},
            new UserValidation{FullName = "Obaskie Sammy",      AccountNumber  = 0123456783,    Email = "sammy@yahoo.co.uk"},
            new UserValidation{FullName = "Ezeh Ceasca",        AccountNumber  = 0123456784,    Email = "ceasca@yahoo.co.uk"},
          };
             
          private static List<UserCredentials> allUserCredentials = new List<UserCredentials>()
          {
            new UserCredentials{Email = "chikki@yahoo.co.uk",   Password = "chikki"},
            new UserCredentials{Email = "dara@yahoo.co.uk",     Password = "dara"},
            new UserCredentials{Email = "lucy@yahoo.co.uk",     Password = "lucy"},
            new UserCredentials{Email = "chikki@yahoo.co.uk",   Password = "ceasca"},
          };

          public static async Task<bool> Login(string email, string password)
          {

                var query = allUserCredentials.Where(e => e.Email == email && e.Password == password);
                var first = await Task.Run<UserCredentials>(query.FirstOrDefault);
                await Task.Delay(2000);
                if (first.Email == email && first.Password == password)
                return true;
                return false;

          }

         public async static Task<string> GetUserValidationDetails(string email)
         {
                var query = allUserValidation.Where(e => e.Email == email);
                var first = await Task.Run(query.FirstOrDefault);
                var profile = "";
                foreach (var item in query)
                {
                    profile += $"{ item.FullName}, {item.AccountNumber}, {item.Email }";
                }
                Console.WriteLine("fetching user report okey!!...");
                return profile;
         }

      }
}

